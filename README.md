# TME Indicator
### Turn a KI 266 DME Indicator into a clock

https://porkfreezer.com/tme

![front](imgs/front.jpg)
![back](imgs/back.jpg)

### ⚠️ PCB has issues ⚠️️

Power supply
 - needs a switch
 - feedback needs to be rerouted
 - needs bigger input capacitors
 - probably shouldn't use USB

KI 266 pins
 - +15V should go to `E` rather than `F`
 - `M` should be connected to ground
