/*
    PIC16F1 library for MCP47CxBxx I2C DACs
    This code is part of the TME Indicator, licensed under the MIT License.

    Copyright (c) 2023 Park Frazer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

#include "dac.h"
#include <xc.h>
#include <stdbool.h>

static void i2c_wait(void) {
    while (!SSP1IF);
    SSP1IF = 0;
}

void dac_write(uint8_t addr, uint16_t data)
{
    SSP1IF = 0;
    SEN = 1;
    i2c_wait();
    SSP1BUF = DAC_ADDR << 1;
    i2c_wait();
    SSP1BUF = (uint8_t)(addr << 3);
    i2c_wait();
    SSP1BUF = data >> 8;
    i2c_wait();
    SSP1BUF = data & 0xFF;
    i2c_wait();
    PEN = 1;
    i2c_wait();
}

void dac_init(void)
{
    SSPCON1 = 0x08 | (I2C_SLEW_CONTROL << 7);
    SSPADD = F_OSC / (4 * F_I2C) - 1;
    SSPEN = 1;
    
    dac_write(DAC_POWER, 0x0F); /* Power off */
    dac_write(DAC_GAIN, 0x000); /* 1x gain */
    dac_write(DAC_VREF, 0x05);  /* Internal 1.214V bandgap */
}