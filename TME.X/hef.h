/*
    PIC high-endurance flash abstraction
    This code is part of the TME Indicator, licensed under the MIT License.

    Copyright (c) 2023 Park Frazer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

#ifndef HEF_H
#define	HEF_H

#include <stdint.h>

#define HEF_ROW_SIZE 32
#define HEF_ADDR_H 0x1F
#define HEF_ADDR_L 0x80

uint8_t hef_read(uint8_t address);
void hef_write_byte(uint8_t address, uint8_t data);
void hef_erase_row(uint8_t address);
void hef_write_row(uint8_t address, uint8_t *data);

#endif	/* HEF_H */

