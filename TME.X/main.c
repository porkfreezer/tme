/*
    
    This code is part of the TME Indicator, licensed under the MIT License.

    Copyright (c) 2023 Park Frazer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection Bits (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover Mode (Internal/External Switchover Mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config CPUDIV = NOCLKDIV// CPU System Clock Selection Bit (NO CPU system divide)
#pragma config USBLSCLK = 24MHz // USB Low Speed Clock Selection bit (System clock expects 24 MHz, FS/LS USB CLKENs divide-by is set to 4.)
#pragma config PLLMULT = 3x     // PLL Multiplier Selection Bit (3x Output Frequency Selected)
#pragma config PLLEN = ENABLED  // PLL Enable Bit (3x or 4x PLL Enabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LPBOR = OFF      // Low-Power Brown Out Reset (Low-Power BOR is disabled)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

#define SETTINGS_VERSION 0x00

#define POWER_PIN C2
#define LED_PIN C4
#define BUTTON_PIN C5

#define CAT_(a, b) a ## b
#define CAT(a, b) CAT_(a, b)
#define DISPLAY_POWER_PORT CAT(R, POWER_PIN)
#define DISPLAY_POWER_TRIS CAT(TRIS, POWER_PIN)
#define LED_PORT CAT(R, LED_PIN)
#define LED_TRIS CAT(TRIS, LED_PIN)
#define BUTTON_PORT CAT(R, BUTTON_PIN)
#define BUTTON_TRIS CAT(TRIS, BUTTON_PIN)

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#include "usb.h"
#include "usb_device.h"
#include "usb_device_cdc.h"

#include "dac.h"
#include "hef.h"

static volatile uint8_t tick = 0;
static volatile bool print = false;
static bool power_enabled = false;
static uint8_t hours = 0, minutes = 0, seconds = 0;
static char time_s[] = "00:00:00\r";
static uint8_t input[32];

typedef union {
    struct {
        uint8_t version;
        uint16_t min_min;
        uint16_t min_range;
        uint16_t hr_min;
        uint16_t hr_range;
    };
    uint8_t buf[32];
} settings_t;

settings_t settings;

static uint16_t map(uint8_t in, uint8_t max_in, uint16_t min_out, uint16_t range)
{
    return (uint16_t)in * range / max_in + min_out;
}

static void set_time(void)
{
    time_s[0] = hours / 10 + '0';
    time_s[1] = hours % 10 + '0';
    time_s[3] = minutes / 10 + '0';
    time_s[4] = minutes % 10 + '0';
    dac_write(DAC_WIPER0, map(hours, 23, settings.hr_min, settings.hr_range));
    dac_write(DAC_WIPER1, map(minutes, 59, settings.min_min, settings.min_range));
}

static uint16_t strint(uint8_t **str)
{
    uint16_t v = 0;
    uint8_t *s = *str;
    while (*s >= '0' && *s <= '9') {
        v = v*10 + (*s - '0');
        ++s;
    }
    *str = s;
    return v;
}

static void display_enable(bool enable)
{
    DISPLAY_POWER_PORT = enable;
    dac_write(DAC_POWER, enable ? 0x00 : 0x0F);
}

static void parse_input(void)
{
    /* s HH:MM: set time*/
    if (input[0] == 's') {
        uint8_t *s = &input[2];
        hours = (uint8_t)strint(&s);
        ++s;
        minutes = (uint8_t)strint(&s);
        seconds = 0;
        set_time();
        print = true;
    }
    /* on/off */
    else if (input[0] == 'o') {
        display_enable(input[1] == 'n');
    }
    /* m <min> <range>: minutes calibration */
    else if (input[0] == 'm') {
        uint8_t *s = &input[2];
        settings.min_min = strint(&s);
        ++s;
        settings.min_range = strint(&s);
        set_time();
    }
    /* h <min> <range>: hours calibration */
    else if (input[0] == 'h') {
        uint8_t *s = &input[2];
        settings.hr_min = strint(&s);
        ++s;
        settings.hr_range = strint(&s);
        set_time();
    }
    /* v: save calibration */
    else if (input[0] == 'v') {
        settings.version = SETTINGS_VERSION;
        hef_erase_row(0);
        hef_write_row(0, settings.buf);
    }
}

void __interrupt() isr(void)
{
    /* 1Hz timer interrupt */
    if (TMR1IF) {
        /* The ISR might not have run exactly at the interrupt, so leave the low
           byte in place. 256 counts is 7.8ms, which shouldn't happen */
        TMR1H = 0x80;
        TMR1IF = 0;
        tick += 1;
        print = true;
    }
}

void main(void)
{
    OSCCON = 0xFC;  /* HFINTOSC @ 16MHz, 3X PLL, PLL enabled */
    ACTCON = 0x90;  /* Active clock tuning enabled for USB */
    
    /* Disable analog, even though the 1454 doesn't have it */
    ANSELA = 0;
    ANSELC = 0;
    
    DISPLAY_POWER_PORT = 0;
    LED_PORT = 0;
    DISPLAY_POWER_TRIS = 0;
    LED_TRIS = 0;
    BUTTON_TRIS = 1;

    T1CON = 0x89;   /* External crystal, 1:1, enabled */
    TMR1 = 0x8000;
    TMR1IF = 0;
    TMR1IE = 1;
    INTCON = 0xC0;  /* Global & peripheral interrupts */
    
    USBDeviceInit();
    USBDeviceAttach();
    
    dac_init();
    set_time();
    
    /* Load saved settings */
    for (uint8_t i = 0; i < sizeof(settings_t); i++)
        settings.buf[i] = hef_read(i);
    
    /* If erased or not properly formatted, set defaults */
    if (settings.version != SETTINGS_VERSION) {
        settings.min_min = 0;
        settings.min_range = 1023;
        settings.hr_min = 0;
        settings.hr_range = 1023;
    }
    
    uint8_t buf_pos = 0;
    uint8_t time_pressed = 0;
    bool usb_connected = false;
    
    while (1) {
        
        USBDeviceTasks();
        
        if (tick) {
            /* Read/reset tick carefully */
            GIE = 0;
            uint8_t tmp_tick = tick;
            tick = 0;
            GIE = 1;
            
            seconds += tmp_tick;
            LED_PORT = seconds & 1;
            
            bool time_changed = false;
            if (BUTTON_PORT == 0) {
                time_pressed += tmp_tick;
                if (time_pressed > 10)
                    hours += 1;
                else if (time_pressed > 5)
                    minutes += 10;
                else
                   minutes += 1;
                time_changed = true;
            }
            
            if (seconds > 3 && !usb_connected && !power_enabled)
                display_enable(true);
            
            if (seconds >= 60) {
                time_changed = true;
                seconds -= 60;
                ++minutes;
            }
            if (minutes >= 60) {
                time_changed = true;
                minutes -= 60;
                if (++hours >= 24)
                    hours = 0;
            }
            
            if (time_changed)
                set_time();
        }
        
        if (BUTTON_PORT == 1)
            time_pressed = 0;
        
        if (USBGetDeviceState() >= CONFIGURED_STATE && !USBIsDeviceSuspended()) {
            usb_connected = true;
            
            CDCTxService();
            
            /* To make sure everything gets transfered, wait for all previous 
               transfers to complete and only send once in this block */
            if (USBUSARTIsTxTrfReady()) {
                if (print) {
                    time_s[6] = seconds / 10 + '0';
                    time_s[7] = seconds % 10 + '0';
                    putUSBUSART((uint8_t *)time_s, sizeof(time_s) - 1);
                    print = false;
                } else {
                    uint8_t read = getsUSBUSART(input+buf_pos, sizeof(input)-buf_pos);
                    if (read) {
                        putUSBUSART(input+buf_pos, read);
                        buf_pos += read;
                        if ((buf_pos > 0 && input[buf_pos-1] == '\r')
                            || buf_pos == sizeof(input)) {
                            buf_pos = 0;
                            parse_input();
                        }
                    }
                }
            }
        }
    }
}

bool USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, uint16_t size)
{
    switch( (int)event ) {

        case EVENT_CONFIGURED:
            CDCInitEP();
            line_coding.bCharFormat = 0;
            line_coding.bDataBits = 8;
            line_coding.bParityType = 0;
            line_coding.dwDTERate = 9600;
            break;

        case EVENT_EP0_REQUEST:
            USBCheckCDCRequest();
            break;

        default:
            break;
    }
    return true;
}
