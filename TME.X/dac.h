/*
    PIC16F1 library for MCP47CxBxx I2C DACs
    This code is part of the TME Indicator, licensed under the MIT License.

    Copyright (c) 2023 Park Frazer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

#ifndef DAC_H
#define	DAC_H

#include <stdint.h>

#ifndef DAC_ADDR
    #define DAC_ADDR 0x60
#endif

#ifndef F_I2C
    #define F_I2C 400
#endif

#ifndef F_OSC
    #define F_OSC 48000
#endif

#ifndef I2C_SLEW_CONTROL
    #define I2C_SLEW_CONTROL 0
#endif

enum {
    DAC_WIPER0 = 0x00,
    DAC_WIPER1 = 0x01,
    DAC_VREF   = 0x08,
    DAC_POWER  = 0x09,
    DAC_GAIN   = 0x0A
};

void dac_init(void);

void dac_write(uint8_t addr, uint16_t data);


#endif

