/*
    PIC high-endurance flash abstraction
    This code is part of the TME Indicator, licensed under the MIT License.

    Copyright (c) 2023 Park Frazer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

#include "hef.h"
#include <stdint.h>
#include <xc.h>

#define PM_UNLOCK() \
    PMCON2 = 0x55; \
    PMCON2 = 0xAA; \
    WR = 1; \
    asm("nop"); \
    asm("nop");

/* Read low byte at address */
uint8_t hef_read(uint8_t address)
{
    PMADRH = 0x1F;
    PMADRL = 0x80 | address;
    CFGS = 0;
    RD = 1;
    asm("nop");
    asm("nop");
    return PMDATL;
}

/* Erase the row that contains address */
void hef_erase_row(uint8_t address)
{
    GIE = 0;
    PMADRH = HEF_ADDR_H;
    PMADRL = HEF_ADDR_L | address;
    CFGS = 0;
    FREE = 1;
    WREN = 1;
    PM_UNLOCK();
    WREN = 0;
    GIE = 1;
}

/* Write data to low byte of address */
void hef_write_byte(uint8_t address, uint8_t data)
{
    GIE = 0;
    CFGS = 0;
    PMADRH = HEF_ADDR_H;
    PMADRL = HEF_ADDR_L | address;
    FREE = 0;
    LWLO = 0;
    WREN = 1;
    PM_UNLOCK();
    WREN = 0;
    GIE = 1;
}

/* Write 32 bytes of data to low bytes of address */
void hef_write_row(uint8_t address, uint8_t *data)
{
    GIE = 0;
    CFGS = 0;
    FREE = 0;
    PMADRH = HEF_ADDR_H;
    LWLO = 1;
    WREN = 1;
    for (uint8_t i = 0; i < 31; i++) {
        PMADRL = (HEF_ADDR_L | address) + i;
        PMDATL = data[i];
        PM_UNLOCK();
    }
    PMADRL = HEF_ADDR_L | address | (HEF_ROW_SIZE - 1);
    PMDATL = data[31];
    LWLO = 0;
    PM_UNLOCK();
    WREN = 0;
    GIE = 1;
}